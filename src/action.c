// ====================================================================================
//                                LICENSE INFORMATIONS
// ====================================================================================
// Copyright (c) 2020-2022 Pawel Piskorz
// Licensed under the Eclipse Public License 2.0
// See attached LICENSE file
// ====================================================================================


// =================================== INCLUDES =======================================
#include <stdlib.h>
#include "action.h"
#include "interrupts.h"


// ============================== PUBLIC DEFINITIONS ==================================

Std_Err action_create(
    Action** self,
    void (*init)(Action*, void*),
    void (*remove)(Action*, void*),
    Std_Err (*step)(Action*, void*, void*),
    uint16_t cache_size
)
{
    if ((*self) != NULL)
    {
        return STD_REFERENCE_ERROR;
    }

    #ifdef _ZXVCV_USE_INTERRUPTS
        irq_disable();
    #endif // _ZXVCV_USE_INTERRUPTS

    *self = (Action*)malloc(sizeof(Action));

    #ifdef _ZXVCV_USE_INTERRUPTS
        irq_enable();
    #endif // _ZXVCV_USE_INTERRUPTS

    if(*self == NULL)
    {
        return STD_ALLOC_ERROR;
    }

    (*self)->step = step;
    (*self)->init = init;
    (*self)->remove = remove;

    (*self)->cache = NULL;
    (*self)->cache_size = cache_size;
    (*self)->state = ACTION_PENDING;

    return STD_OK;
}

Std_Err action_delete(Action** self)
{
    if((*self) == NULL)
    {
        return STD_REFERENCE_ERROR;
    }

    action_terminate(*self);

    #ifdef _ZXVCV_USE_INTERRUPTS
        irq_disable();
    #endif // _ZXVCV_USE_INTERRUPTS

    free(*self);

    #ifdef _ZXVCV_USE_INTERRUPTS
        irq_enable();
    #endif // _ZXVCV_USE_INTERRUPTS

    *self = NULL;

    return STD_OK;
}

Std_Err action_execute(Action* self, void* params)
{
    if (self == NULL)
    {
        return STD_REFERENCE_ERROR;
    }

    Std_Err err;
    switch (action_get_state(self))
    {
        case ACTION_STOPPED:
            return STD_NOOPERATION_ERROR;

        case ACTION_TERMINATED:
            return STD_TERMINATION_ERROR;

        case ACTION_PAUSED:
            return STD_OK;

        case ACTION_PENDING:
            if(self->cache_size > 0)
            {
                #ifdef _ZXVCV_USE_INTERRUPTS
                    irq_disable();
                #endif // _ZXVCV_USE_INTERRUPTS

                self->cache = (void*)malloc(self->cache_size);

                if(self->cache == NULL)
                {
                    return STD_ALLOC_ERROR;
                }

                #ifdef _ZXVCV_USE_INTERRUPTS
                    irq_enable();
                #endif // _ZXVCV_USE_INTERRUPTS
            }

            self->state = ACTION_WORKING;

            if(self->init != NULL)
            {
                self->init(self, self->cache);
            }
            return STD_OK;

        case ACTION_WORKING:
            err = self->step(self, params, self->cache);

            if(action_get_state(self) == ACTION_TERMINATED)
            {
                return STD_TERMINATION_ERROR;
            }
            return err;

        default:
            return STD_ERROR;
    }
}

ActionStatus action_get_state(Action* self)
{
    if(self == NULL)
    {
        return ACTION_UNKNOWN;
    }

    return self->state;
}

void action_start(Action* self)
{
    if(self == NULL) return;

    if(self->state == ACTION_PAUSED)
    {
        self->state = ACTION_WORKING;
    }
}

void action_pause(Action* self)
{
    if(self == NULL) return;

    if(self->state == ACTION_WORKING)
    {
        self->state = ACTION_PAUSED;
    }
}

void action_stop(Action* self)
{
    if(self == NULL) return;

    switch (action_get_state(self))
    {
        case ACTION_TERMINATED:
            return;

        case ACTION_PAUSED:
        case ACTION_WORKING:
            if(self->remove != NULL)
            {
                self->remove(self, self->cache);
            }

            if(self->cache_size > 0)
            {
                #ifdef _ZXVCV_USE_INTERRUPTS
                    irq_disable();
                #endif // _ZXVCV_USE_INTERRUPTS

                free(self->cache);

                #ifdef _ZXVCV_USE_INTERRUPTS
                    irq_enable();
                #endif // _ZXVCV_USE_INTERRUPTS

                self->cache = NULL;
            }

        case ACTION_PENDING:
        case ACTION_STOPPED:
        default:
            break;
    }

    self->state = ACTION_STOPPED;
}

void action_restart(Action* self)
{
    if(self == NULL) return;

    action_stop(self);
    self->state = ACTION_PENDING;
}

void action_terminate(Action* self)
{
    if(self == NULL) return;

    action_stop(self);
    self->state = ACTION_TERMINATED;
}
