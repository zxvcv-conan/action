


# API

[comment]: # ([[_DOCMD_USER_BLOCK_0_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_0_END]])

</br>

## Structures
***

[comment]: # ([[_DOCMD_USER_BLOCK_1_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_1_END]])

> ## Action_Tag
> ***
> Action object.
>
> Object that is a single action which coud be executded in many steps.
>
> ### <u><i>Members:</i></u>
> |NAME|TYPE|DESCRIPTION|
> |-|-|-|
> |init|void*||
> |remove|void*||
> |step|Std_Err*||
> |cache|void*||
> |cache_size|uint16_t||
> |state|ActionStatus||

</br>




## Enums
***

[comment]: # ([[_DOCMD_USER_BLOCK_3_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_3_END]])

> ## ActionStatus_Tag
> ***
> Action status (state) enumerator.
>
> Contains easy to use list of states in which action could be.
>
> ### <u><i>Values:</i></u>
> |NAME|VALUE|DESCRIPTION|
> |-|-|-|
> |ACTION_STOPPED|0|Ended|
> |ACTION_PAUSED|1| |
> |ACTION_WORKING|2|Initialized|
> |ACTION_PENDING|3|Not initialized|
> |ACTION_TERMINATED|4| |
> |ACTION_UNKNOWN|5| |

</br>



## Typedefs
***

[comment]: # ([[_DOCMD_USER_BLOCK_4_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_4_END]])

> ## ActionStatus
> ***
> Action status type.
>
> ### <u><i>Definition:</i></u>
> ActionStatus_Tag
>

</br>

> ## Action
> ***
> Action type.
>
> ### <u><i>Definition:</i></u>
> Action_Tag
>

</br>



</br>

## Functions
***

[comment]: # ([[_DOCMD_USER_BLOCK_5_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_5_END]])

> ## action_create
> ***
> Create action object.
>
> Every use of 'action_create' require use 'action_delete' at the end.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Action**|out|Placeholder for created action object.|
> |init|void*|in|Pointer to function that will be an initilizer for action.This function will be called before anything performed on action.|
> |remove|void*|in|Pointer to function that will be a destructor for action.This function will be called on action destroy.|
> |step|Std_Err*|in|Function that will be called every step for action.|
> |cache_size|uint16_t|in|Size of cache that will be allocated andcould be used in init, remove and step functions.|
> ### <u><i>Return:</i></u>
> (Std_Err) An enum object that contains the exit code.
> |CODE|DESCRIPTION|
> |-|-|
> |STD_OK|Execution success.|
> |STD_REFERENCE_ERROR|Passed self placeholder is NULL (not initialized).|
> |STD_ALLOC_ERROR|When memory alocation fails.|

</br>

> ## action_delete
> ***
> Delete action object.
>
> Every use of 'action_delete' require use 'action_create' before.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Action**|in|Action object to destroy.|
> ### <u><i>Return:</i></u>
> (Std_Err) An enum object that contains the exit code.
> |CODE|DESCRIPTION|
> |-|-|
> |STD_OK|Execution success.|
> |STD_REFERENCE_ERROR|Passed self placeholder is NULL (not initialized).|

</br>

> ## action_execute
> ***
> Delete action object.
>
> Every use of 'action_delete' require use 'action_create' before.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Action*|in|Action object to execute.|
> |params|void*|in|Pointer to parameters object, passed directly to step functon when executing.|
> ### <u><i>Return:</i></u>
> (Std_Err) An enum object that contains the exit code.
> |CODE|DESCRIPTION|
> |-|-|
> |STD_OK|Execution success (when action in state ACTION_PAUSED, ACTION_PENDING or ACTION_WORKING).|
> |STD_REFERENCE_ERROR|Passed self placeholder is NULL (not initialized).|
> |STD_ALLOC_ERROR|When memory alocation fails.|
> |STD_NOOPERATION_ERROR|When action in state ACTION_STOPPED.|
> |STD_TERMINATION_ERROR|When action in state ACTION_TERMINATED.|
> |STD_ERROR|When action in unknown state (this should never happend).|

</br>

> ## action_get_state
> ***
> Returns current state of the action.
>
> 
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Action*|in|Action object.|
> ### <u><i>Return:</i></u>
> (ActionStatus) Current state of the action.

</br>

> ## action_start
> ***
> Starts the action execution.
>
> Changes state of the action from ACTION_PAUSED to ACTION_PENDING.<br>If action is in other state than ACTION_PAUSED, this function will do nothing.<br>If self is not initialized, this function will do nothing.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Action*|in|Action object.|

</br>

> ## action_pause
> ***
> Pauses the action execution.
>
> Changes state of the action from ACTION_WORKING to ACTION_PAUSED.<br>This will cause that running action_execute will skip executing action step.<br>If action is in other state than ACTION_WORKING, this function will do nothing.<br>If self is not initialized, this function will do nothing.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Action*|in|Action object.|

</br>

> ## action_stop
> ***
> Stops the action execution.
>
> Changes stateto ACTION_STOPPED if action was in other state than ACTION_TERMINATED.<br>If action is in state ACTION_TERMINATED, this function will fo nothing.<br>When action was at the begining in state ACTION_WORKING or ACTION_PAUSED, deinitializationwill be invoked (remove method) and cache will be unallocated.<br>If self is not initialized, this function will do nothing.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Action*|in|Action object.|

</br>

> ## action_restart
> ***
> Perform restart of the action.
>
> Invokes action_stop(), and changes state into ACTION_PENDING.<br>If self is not initialized, this function will do nothing.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Action*|in|Action object.|

</br>

> ## action_terminate
> ***
> Terminates the action.
>
> Invokes action_stop(), and changes state into ACTION_TERMINATED.<br>If self is not initialized, this function will do nothing.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Action*|in|Action object.|

</br>



[comment]: # ([[_DOCMD_USER_BLOCK_6_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_6_END]])

***
[Main page](../README.md) | [Repository url](https://gitlab.com/zxvcv-conan/action) </br>
***
<i>Generated with <b>docmd</b> Python package.</i>
