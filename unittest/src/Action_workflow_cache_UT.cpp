
#include "gtest/gtest.h"

extern "C"
{
    #include "errors.h"
    #include "action.h"

    void test_cache_init(Action* action, void* cache)
    {
        *(uint8_t*)cache = 0;
    }

    void test_cache_remove(Action* action, void* cache){}

    // action that will forever add passed value into cached value
    Std_Err test_cache_step(Action* action, void* params, void* cache)
    {
        *(uint8_t*)cache += *(uint8_t*)params;
        return STD_OK;
    }
}

class Action_workflow_cache_UT: public ::testing::Test {
public:
    Action_workflow_cache_UT(){}
    ~Action_workflow_cache_UT(){}

    virtual void SetUp()
    {
        ASSERT_EQ(action, nullptr);

        err = action_create(&action, test_cache_init, test_cache_remove, test_cache_step, sizeof(uint8_t));

        EXPECT_EQ(err, STD_OK);
        EXPECT_EQ(action->cache, nullptr);
        EXPECT_EQ(action->cache_size, sizeof(uint8_t));
        EXPECT_EQ(action->state, ACTION_PENDING);
        EXPECT_EQ(action->init, test_cache_init);
        EXPECT_EQ(action->remove, test_cache_remove);
        EXPECT_EQ(action->step, test_cache_step);
    }

    virtual void TearDown()
    {
        err = action_delete(&action);

        EXPECT_EQ(err, STD_OK);
        EXPECT_EQ(action, nullptr);
    }

    Action* action = NULL;
    Std_Err err;
};

/************************** TESTS **************************/

TEST_F(Action_workflow_cache_UT, action_execute_from_stopped)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_stop(action);
    EXPECT_EQ(action_get_state(action), ACTION_STOPPED);
    EXPECT_EQ(action->cache, nullptr);

    err = action_execute(action, (void*)(&param));

    EXPECT_EQ(err, STD_NOOPERATION_ERROR);
    EXPECT_EQ(action_get_state(action), ACTION_STOPPED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_workflow_cache_UT, action_execute_from_paused)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_pause(action);
    EXPECT_EQ(action_get_state(action), ACTION_PAUSED);
    EXPECT_NE(action->cache, nullptr);
    EXPECT_EQ(*((uint8_t*)(action->cache)), 0);

    err = action_execute(action, (void*)(&param));

    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(action_get_state(action), ACTION_PAUSED);
    EXPECT_NE(action->cache, nullptr);
    EXPECT_EQ(*((uint8_t*)(action->cache)), 0);
}

TEST_F(Action_workflow_cache_UT, action_execute_from_working)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    EXPECT_EQ(action_get_state(action), ACTION_WORKING);
    EXPECT_NE(action->cache, nullptr);
    EXPECT_EQ(*((uint8_t*)(action->cache)), 0);

    err = action_execute(action, (void*)(&param));

    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(action_get_state(action), ACTION_WORKING);
    EXPECT_NE(action->cache, nullptr);
    EXPECT_EQ(*((uint8_t*)(action->cache)), 5);
}

TEST_F(Action_workflow_cache_UT, action_execute_from_pending)
{
    uint8_t param = 5;
    EXPECT_EQ(action_get_state(action), ACTION_PENDING);
    EXPECT_EQ(action->cache, nullptr);

    err = action_execute(action, (void*)(&param));

    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(action_get_state(action), ACTION_WORKING);
    EXPECT_NE(action->cache, nullptr);
    EXPECT_EQ(*((uint8_t*)(action->cache)), 0);
}

TEST_F(Action_workflow_cache_UT, action_execute_from_terminated)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_terminate(action);
    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);

    err = action_execute(action, (void*)(&param));

    EXPECT_EQ(err, STD_TERMINATION_ERROR);
    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_workflow_cache_UT, action_execute_multiple_run)
{
    uint8_t param1 = 5;
    uint8_t param2 = 7;

    EXPECT_EQ(action_get_state(action), ACTION_PENDING);

    err = action_execute(action, (void*)(&param1));
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(action_get_state(action), ACTION_WORKING);
    EXPECT_EQ(*((uint8_t*)(action->cache)), 0);

    err = action_execute(action, (void*)(&param1));
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(action_get_state(action), ACTION_WORKING);
    EXPECT_EQ(*((uint8_t*)(action->cache)), 5);

    err = action_execute(action, (void*)(&param2));
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(action_get_state(action), ACTION_WORKING);
    EXPECT_EQ(*(uint8_t*)(action->cache), 12);
}
