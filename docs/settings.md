


# Settings

- os
- compiler
- build_type
- arch
- fpu

[comment]: # ([[_DOCMD_USER_BLOCK_0_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_0_END]])

</br>

# Options

|NAME|AVAILABLE VALUES|DEFAULT VALUE|
|-|-|-|
|shared|[True, False]|False|
|interrupts_safe|[True, False]|False|

[comment]: # ([[_DOCMD_USER_BLOCK_1_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_1_END]])

</br>

# Requires

- errors/[<1.0.0]@zxvcv-conan+errors/testing
- interrupts/[<1.0.0]@zxvcv-conan+interrupts/testing

[comment]: # ([[_DOCMD_USER_BLOCK_2_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_2_END]])

***
[Main page](../README.md) | [Repository url](https://gitlab.com/zxvcv-conan/action) </br>
***
<i>Generated with <b>docmd</b> Python package.</i>
