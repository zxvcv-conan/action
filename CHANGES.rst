Changelog
=========

0.1.4 (2022-11-06)
------------------
- Documentation generated.
- action.h file comments restructurization.
- Doc comments update.
- README.md updates.

0.1.3 (2022-10-17)
------------------
- Add unittests.
- Add action ACTION_UNKNOWN.
- Source code logic fixes.

0.1.2 (2022-10-15)
------------------
- Add init and remove methods.
- Add params for running action.
- Rename data -> cache.
- First UT added.

0.1.1 (2022-10-15)
------------------
- Add template for unittests.
- Add profiles.
- Fixes - build working fine.

0.1.0 (2022-10-15)
------------------
- Initial.