
#include "gtest/gtest.h"

extern "C"
{
    #include "errors.h"
    #include "action.h"
}

class Action_creation_UT: public ::testing::Test {
public:
    Action_creation_UT(){}
    ~Action_creation_UT(){}

    virtual void SetUp(){}

    virtual void TearDown(){}

    Action* action = NULL;
    Std_Err err;
};

extern "C"
{
    void test_init(Action* action, void* cache)
    {
        *(uint8_t*)cache == 0;
    }

    // action that will forever add passed value into cached value
    Std_Err test_step(Action* action, void* params, void* cache)
    {
        *(uint8_t*)cache += *(uint8_t*)params;
        return STD_OK;
    }
}

/************************** TESTS **************************/

TEST_F(Action_creation_UT, action_create)
{
    EXPECT_EQ(action, nullptr);

    err = action_create(&action, test_init, NULL, test_step, sizeof(uint8_t));

    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(action->cache, nullptr);
    EXPECT_EQ(action->cache_size, sizeof(uint8_t));
    EXPECT_EQ(action->state, ACTION_PENDING);
    EXPECT_EQ(action->init, test_init);
    EXPECT_EQ(action->remove, nullptr);
    EXPECT_EQ(action->step, test_step);

    err = action_delete(&action);

    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(action, nullptr);
}

TEST_F(Action_creation_UT, action_create_already_created)
{
    EXPECT_EQ(action, nullptr);

    err = action_create(&action, test_init, NULL, test_step, sizeof(uint8_t));
    EXPECT_EQ(err, STD_OK);

    err = action_create(&action, test_init, NULL, test_step, sizeof(uint8_t));
    EXPECT_EQ(err, STD_REFERENCE_ERROR);

    err = action_delete(&action);

    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(action, nullptr);
}

TEST_F(Action_creation_UT, action_delete_not_created)
{
    EXPECT_EQ(action, nullptr);

    err = action_delete(&action);

    EXPECT_EQ(err, STD_REFERENCE_ERROR);
    EXPECT_EQ(action, nullptr);
}

TEST_F(Action_creation_UT, action_execute_not_created)
{
    EXPECT_EQ(action, nullptr);

    err = action_execute(action, nullptr);

    EXPECT_EQ(err, STD_REFERENCE_ERROR);
    EXPECT_EQ(action, nullptr);
}

TEST_F(Action_creation_UT, action_get_state_not_created)
{
    EXPECT_EQ(action, nullptr);

    ActionStatus status = action_get_state(action);

    EXPECT_EQ(status, ACTION_UNKNOWN);
    EXPECT_EQ(action, nullptr);
}

TEST_F(Action_creation_UT, action_start_not_created)
{
    EXPECT_EQ(action, nullptr);

    action_start(action);

    EXPECT_EQ(action, nullptr);
}

TEST_F(Action_creation_UT, action_pause_not_created)
{
    EXPECT_EQ(action, nullptr);

    action_pause(action);

    EXPECT_EQ(action, nullptr);
}

TEST_F(Action_creation_UT, action_stop_not_created)
{
    EXPECT_EQ(action, nullptr);

    action_stop(action);

    EXPECT_EQ(action, nullptr);
}

TEST_F(Action_creation_UT, action_restart_not_created)
{
    EXPECT_EQ(action, nullptr);

    action_restart(action);

    EXPECT_EQ(action, nullptr);
}

TEST_F(Action_creation_UT, action_terminate_not_created)
{
    EXPECT_EQ(action, nullptr);

    action_terminate(action);

    EXPECT_EQ(action, nullptr);
}
