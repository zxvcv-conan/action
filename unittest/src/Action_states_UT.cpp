
#include "gtest/gtest.h"

extern "C"
{
    #include "errors.h"
    #include "action.h"

    void test_states_init(Action* action, void* cache)
    {
        *(uint8_t*)cache = 0;
    }

    void test_states_remove(Action* action, void* cache){}

    // action that will forever add passed value into cached value
    Std_Err test_states_step(Action* action, void* params, void* cache)
    {
        *(uint8_t*)cache += *(uint8_t*)params;
        return STD_OK;
    }
}

class Action_states_UT: public ::testing::Test {
public:
    Action_states_UT(){}
    ~Action_states_UT(){}

    virtual void SetUp()
    {
        ASSERT_EQ(action, nullptr);

        err = action_create(&action, test_states_init, test_states_remove, test_states_step, sizeof(uint8_t));

        EXPECT_EQ(err, STD_OK);
        EXPECT_EQ(action->cache, nullptr);
        EXPECT_EQ(action->cache_size, sizeof(uint8_t));
        EXPECT_EQ(action->state, ACTION_PENDING);
        EXPECT_EQ(action->init, test_states_init);
        EXPECT_EQ(action->remove, test_states_remove);
        EXPECT_EQ(action->step, test_states_step);
    }

    virtual void TearDown()
    {
        err = action_delete(&action);

        EXPECT_EQ(err, STD_OK);
        EXPECT_EQ(action, nullptr);
    }

    Action* action = NULL;
    Std_Err err;
};

/************************** TESTS **************************/

TEST_F(Action_states_UT, action_start_from_stopped)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_stop(action);
    EXPECT_EQ(action_get_state(action), ACTION_STOPPED);
    EXPECT_EQ(action->cache, nullptr);

    action_start(action);

    EXPECT_EQ(action_get_state(action), ACTION_STOPPED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_start_from_paused)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_pause(action);
    EXPECT_EQ(action_get_state(action), ACTION_PAUSED);
    EXPECT_NE(action->cache, nullptr);

    action_start(action);

    EXPECT_EQ(action_get_state(action), ACTION_WORKING);
    EXPECT_NE(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_start_from_working)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    EXPECT_EQ(action_get_state(action), ACTION_WORKING);
    EXPECT_NE(action->cache, nullptr);

    action_start(action);

    EXPECT_EQ(action_get_state(action), ACTION_WORKING);
    EXPECT_NE(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_start_from_pending)
{
    EXPECT_EQ(action_get_state(action), ACTION_PENDING);
    EXPECT_EQ(action->cache, nullptr);

    action_start(action);

    EXPECT_EQ(action_get_state(action), ACTION_PENDING);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_start_from_terminated)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_terminate(action);
    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);

    action_start(action);

    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_pause_from_stopped)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_stop(action);
    EXPECT_EQ(action_get_state(action), ACTION_STOPPED);
    EXPECT_EQ(action->cache, nullptr);

    action_pause(action);

    EXPECT_EQ(action_get_state(action), ACTION_STOPPED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_pause_from_paused)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_pause(action);
    EXPECT_EQ(action_get_state(action), ACTION_PAUSED);
    EXPECT_NE(action->cache, nullptr);

    action_pause(action);

    EXPECT_EQ(action_get_state(action), ACTION_PAUSED);
    EXPECT_NE(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_pause_from_working)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    EXPECT_EQ(action_get_state(action), ACTION_WORKING);
    EXPECT_NE(action->cache, nullptr);

    action_pause(action);

    EXPECT_EQ(action_get_state(action), ACTION_PAUSED);
    EXPECT_NE(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_pause_from_pending)
{
    EXPECT_EQ(action_get_state(action), ACTION_PENDING);
    EXPECT_EQ(action->cache, nullptr);

    action_pause(action);

    EXPECT_EQ(action_get_state(action), ACTION_PENDING);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_pause_from_terminated)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_terminate(action);
    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);

    action_pause(action);

    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_stop_from_stopped)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_stop(action);
    EXPECT_EQ(action_get_state(action), ACTION_STOPPED);
    EXPECT_EQ(action->cache, nullptr);

    action_stop(action);

    EXPECT_EQ(action_get_state(action), ACTION_STOPPED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_stop_from_paused)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_pause(action);
    EXPECT_EQ(action_get_state(action), ACTION_PAUSED);
    EXPECT_NE(action->cache, nullptr);

    action_stop(action);

    EXPECT_EQ(action_get_state(action), ACTION_STOPPED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_stop_from_working)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    EXPECT_EQ(action_get_state(action), ACTION_WORKING);
    EXPECT_NE(action->cache, nullptr);

    action_stop(action);

    EXPECT_EQ(action_get_state(action), ACTION_STOPPED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_stop_from_pending)
{
    EXPECT_EQ(action_get_state(action), ACTION_PENDING);
    EXPECT_EQ(action->cache, nullptr);

    action_stop(action);

    EXPECT_EQ(action_get_state(action), ACTION_STOPPED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_stop_from_terminated)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_terminate(action);
    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);

    action_stop(action);

    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_restart_from_stopped)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_stop(action);
    EXPECT_EQ(action_get_state(action), ACTION_STOPPED);
    EXPECT_EQ(action->cache, nullptr);

    action_restart(action);

    EXPECT_EQ(action_get_state(action), ACTION_PENDING);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_restart_from_paused)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_pause(action);
    EXPECT_EQ(action_get_state(action), ACTION_PAUSED);
    EXPECT_NE(action->cache, nullptr);

    action_restart(action);

    EXPECT_EQ(action_get_state(action), ACTION_PENDING);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_restart_from_working)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    EXPECT_EQ(action_get_state(action), ACTION_WORKING);
    EXPECT_NE(action->cache, nullptr);

    action_restart(action);

    EXPECT_EQ(action_get_state(action), ACTION_PENDING);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_restart_from_pending)
{
    EXPECT_EQ(action_get_state(action), ACTION_PENDING);
    EXPECT_EQ(action->cache, nullptr);

    action_restart(action);

    EXPECT_EQ(action_get_state(action), ACTION_PENDING);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_restart_from_terminated)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_terminate(action);
    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);

    action_restart(action);

    EXPECT_EQ(action_get_state(action), ACTION_PENDING);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_terminate_from_stopped)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_stop(action);
    EXPECT_EQ(action_get_state(action), ACTION_STOPPED);
    EXPECT_EQ(action->cache, nullptr);

    action_terminate(action);

    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_terminate_from_paused)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_pause(action);
    EXPECT_EQ(action_get_state(action), ACTION_PAUSED);
    EXPECT_NE(action->cache, nullptr);

    action_terminate(action);

    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_terminate_from_working)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    EXPECT_EQ(action_get_state(action), ACTION_WORKING);
    EXPECT_NE(action->cache, nullptr);

    action_terminate(action);

    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_terminate_from_pending)
{
    EXPECT_EQ(action_get_state(action), ACTION_PENDING);
    EXPECT_EQ(action->cache, nullptr);

    action_terminate(action);

    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_terminate_from_terminated)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_terminate(action);
    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);

    action_terminate(action);

    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);
}

TEST_F(Action_states_UT, action_get_state_from_stopped)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_stop(action);
    EXPECT_EQ(action_get_state(action), ACTION_STOPPED);
    EXPECT_EQ(action->cache, nullptr);

    EXPECT_EQ(action_get_state(action), ACTION_STOPPED);
}

TEST_F(Action_states_UT, action_get_state_from_paused)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_pause(action);
    EXPECT_EQ(action_get_state(action), ACTION_PAUSED);
    EXPECT_NE(action->cache, nullptr);

    EXPECT_EQ(action_get_state(action), ACTION_PAUSED);
}

TEST_F(Action_states_UT, action_get_state_from_working)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    EXPECT_EQ(action_get_state(action), ACTION_WORKING);
    EXPECT_NE(action->cache, nullptr);

    EXPECT_EQ(action_get_state(action), ACTION_WORKING);
}

TEST_F(Action_states_UT, action_get_state_from_pending)
{
    EXPECT_EQ(action_get_state(action), ACTION_PENDING);
    EXPECT_EQ(action->cache, nullptr);

    EXPECT_EQ(action_get_state(action), ACTION_PENDING);
}

TEST_F(Action_states_UT, action_get_state_from_terminated)
{
    uint8_t param = 5;
    err = action_execute(action, (void*)(&param));
    action_terminate(action);
    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
    EXPECT_EQ(action->cache, nullptr);

    EXPECT_EQ(action_get_state(action), ACTION_TERMINATED);
}
