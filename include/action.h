// ====================================================================================
//                                LICENSE INFORMATIONS
// ====================================================================================
// Copyright (c) 2020-2022 Pawel Piskorz
// Licensed under the Eclipse Public License 2.0
// See attached LICENSE file
// ====================================================================================

#ifndef ZXVCV_ACTION_H_
#define ZXVCV_ACTION_H_


// =================================== INCLUDES =======================================
#include <stddef.h>
#include <stdint.h>
#include "errors.h"


// ================================== DATA TYPES ======================================

/** Action status type. */
typedef
/** Action status (state) enumerator.
 *
 * Contains easy to use list of states in which action could be.
 */
enum ActionStatus_Tag{
    /** Ended */
    ACTION_STOPPED      = 0,
    ACTION_PAUSED       = 1,

    /** Initialized */
    ACTION_WORKING      = 2,

    /** Not initialized */
    ACTION_PENDING      = 3,
    ACTION_TERMINATED   = 4,
    ACTION_UNKNOWN      = 5
}ActionStatus;

/** Action type. */
typedef
/** Action object.
 *
 * Object that is a single action which coud be executded in many steps.
 */
struct Action_Tag{
    void (*init)(struct Action_Tag*, void*);
    void (*remove)(struct Action_Tag*, void*);
    Std_Err (*step)(struct Action_Tag*, void*, void*);

    void* cache;
    uint16_t cache_size;

    ActionStatus state;
}Action;


// ============================== PUBLIC DECLARATIONS =================================

/** Create action object.
 *
 * Every use of 'action_create' require use 'action_delete' at the end.
 *
 * @param self [out] Placeholder for created action object.
 * @param init [in] Pointer to function that will be an initilizer for action.
 *                  This function will be called before anything performed on action.
 * @param remove [in] Pointer to function that will be a destructor for action.
 *                    This function will be called on action destroy.
 * @param step [in] Function that will be called every step for action.
 * @param cache_size [in] Size of cache that will be allocated and
 *                        could be used in init, remove and step functions.
 *
 * @return An enum object that contains the exit code.
 * STD_OK Execution success.
 * STD_REFERENCE_ERROR Passed self placeholder is NULL (not initialized).
 * STD_ALLOC_ERROR When memory alocation fails.
 */
Std_Err action_create(
    Action** self,
    void (*init)(Action*, void*),
    void (*remove)(Action*, void*),
    Std_Err (*step)(Action*, void*, void*),
    uint16_t cache_size
);

/** Delete action object.
 *
 * Every use of 'action_delete' require use 'action_create' before.
 *
 * @param self [in] Action object to destroy.
 *
 * @return An enum object that contains the exit code.
 * STD_OK Execution success.
 * STD_REFERENCE_ERROR Passed self placeholder is NULL (not initialized).
 */
Std_Err action_delete(Action** self);

/** Delete action object.
 *
 * Every use of 'action_delete' require use 'action_create' before.
 *
 * @param self [in] Action object to execute.
 * @param params [in] Pointer to parameters object, passed directly to step functon when executing.
 *
 * @return An enum object that contains the exit code.
 * STD_OK Execution success (when action in state ACTION_PAUSED, ACTION_PENDING or ACTION_WORKING).
 * STD_REFERENCE_ERROR Passed self placeholder is NULL (not initialized).
 * STD_ALLOC_ERROR When memory alocation fails.
 * STD_NOOPERATION_ERROR When action in state ACTION_STOPPED.
 * STD_TERMINATION_ERROR When action in state ACTION_TERMINATED.
 * STD_ERROR When action in unknown state (this should never happend).
 */
Std_Err action_execute(Action* self, void* params);

/** Returns current state of the action.
 *
 * @param self [in] Action object.
 *
 * @return Current state of the action.
 */
ActionStatus action_get_state(Action* self);

/** Starts the action execution.
 *
 * Changes state of the action from ACTION_PAUSED to ACTION_PENDING.<br>
 * If action is in other state than ACTION_PAUSED, this function will do nothing.<br>
 * If self is not initialized, this function will do nothing.
 *
 * @param self [in] Action object.
 */
void action_start(Action* self);

/** Pauses the action execution.
 *
 * Changes state of the action from ACTION_WORKING to ACTION_PAUSED.<br>
 * This will cause that running action_execute will skip executing action step.<br>
 * If action is in other state than ACTION_WORKING, this function will do nothing.<br>
 * If self is not initialized, this function will do nothing.
 *
 * @param self [in] Action object.
 */
void action_pause(Action* self);

/** Stops the action execution.
 *
 * Changes stateto ACTION_STOPPED if action was in other state than ACTION_TERMINATED.<br>
 * If action is in state ACTION_TERMINATED, this function will fo nothing.<br>
 * When action was at the begining in state ACTION_WORKING or ACTION_PAUSED, deinitialization
 * will be invoked (remove method) and cache will be unallocated.<br>
 * If self is not initialized, this function will do nothing.
 *
 * @param self [in] Action object.
 */
void action_stop(Action* self);

/** Perform restart of the action.
 *
 * Invokes action_stop(), and changes state into ACTION_PENDING.<br>
 * If self is not initialized, this function will do nothing.
 *
 * @param self [in] Action object.
 */
void action_restart(Action* self);

/** Terminates the action.
 *
 * Invokes action_stop(), and changes state into ACTION_TERMINATED.<br>
 * If self is not initialized, this function will do nothing.
 *
 * @param self [in] Action object.
 */
void action_terminate(Action* self);

#endif // ZXVCV_ACTION_H_
