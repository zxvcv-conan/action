


# action

Component for executing any action with non blocking way.

[comment]: # ([[_DOCMD_USER_BLOCK_0_BEGIN]])
Allow to create action object, and execute it step by step (implementing and writing steps function, initializer function and destructor functoin if fully on the user site).<br>
Code is written using conan C/C++ package manager. Thanks to that it's reusable easily between projects.

Code is written to support multiple platforms:
- Linux
- ARM microcontrolers.
- AVR microcontrolers.

For building there are prepared Docker images - for each platform.

With minimal effort code could be used also together with interrupts.

[comment]: # ([[_DOCMD_USER_BLOCK_0_END]])

***

[comment]: # ([[_DOCMD_USER_BLOCK_1_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_1_END]])

</br>

## Table of contents
***
- [Package Settings](docs/settings.md) - parameters, settings and defines and requirements description
- [API reference](docs/api.md) - api description for public interface
- [Building and Testing](docs/build_test.md) - how to build and test package
- [Usage examples](docs/examples.md)
- [Known Bugs](docs/bugs.md)

</br>

## License informations
***
Eclipse Public License - v 2.0

[comment]: # ([[_DOCMD_USER_BLOCK_2_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_2_END]])

</br>

## Authors
***
- Pawel Piskorz ppiskorz0@gmail.com

[comment]: # ([[_DOCMD_USER_BLOCK_3_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_3_END]])

</br>

***
[Main page](./README.md) | [Repository url](https://gitlab.com/zxvcv-conan/action) </br>
***
<i>Generated with <b>docmd</b> Python package.</i>

